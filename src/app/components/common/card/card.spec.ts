import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardComponent } from './card.component';

describe('CardComponent', () => {
  let component: CardComponent;
  let fixture: ComponentFixture<CardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render title', () => {
    component.name = 'test';
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.name').textContent).toContain('test');
  });

  it('should render age', () => {
    component.age = 49;
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.age').textContent).toContain('49');
  });

  it('should have an ability to use click output', () => {
    component.age = 49;
    component.clickAge.subscribe(value => {
      expect(value).toBe(49);
    });
    fixture.detectChanges();
    const element = fixture.nativeElement.querySelector('.age');
    element.click();
  });
});
