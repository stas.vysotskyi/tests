import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Params, Response } from './users.types';

@Injectable()
export class UsersRestService {

  constructor(private http: HttpClient) {}

  getUsersList(params: Params): Observable<Response[]> {
    return this.http.get<Response[]>('/users', { params });
  }
}
