import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.html',
  styleUrls: ['./card.scss']
})
export class CardComponent {

  @Input()
  name: string;

  @Input()
  age: number;

  @Output()
  clickAge = new EventEmitter<number>();
}
