import { Component, OnInit } from '@angular/core';

import { UsersRestService } from '../../rest/users/users.service';
import { Response as User } from '../../rest/users/users.types';

@Component({
  templateUrl: './home.html',
  styleUrls: ['./home.scss'],
  providers: [UsersRestService]
})
export class HomeComponent implements OnInit {

  users: User[] = [];

  constructor(private usersRestService: UsersRestService) {}

  ngOnInit(): void {
    this.usersRestService.getUsersList({ group: 'test' })
      .subscribe(users => this.users = users);
  }
}
