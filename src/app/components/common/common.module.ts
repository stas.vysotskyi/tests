import { NgModule } from '@angular/core';
import { CommonModule as AngularCommonModule } from '@angular/common';

import { CardComponent } from './card/card.component';

@NgModule({
  declarations: [CardComponent],
  imports: [AngularCommonModule],
  exports: [CardComponent]
})
export class CommonModule {}
