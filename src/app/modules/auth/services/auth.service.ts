import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {

  private key = 'token';

  getToken(): string {
    return localStorage.getItem(this.key);
  }

  saveToken(token?: string): void {
    localStorage.setItem(this.key, token);
  }

  removeToken(): void {
    localStorage.removeItem(this.key);
  }

  isAuthorized(): boolean {
    return !!localStorage.getItem(this.key);
  }
}
