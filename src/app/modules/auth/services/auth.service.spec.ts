import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';

let store = {};

const mockLocalStorage = {
  getItem: (key: string): string => {
    return key in store ? store[key] : null;
  },
  setItem: (key: string, value: string) => {
    store[key] = `${value}`;
  },
  removeItem: (key: string) => {
    delete store[key];
  },
  clear: () => {
    store = {};
  }
};

describe('AuthService', () => {
  const key = 'token';

  let service: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthService]
    });
    service = TestBed.inject(AuthService);

    spyOn(localStorage, 'getItem')
      .and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, 'setItem')
      .and.callFake(mockLocalStorage.setItem);
    spyOn(localStorage, 'removeItem')
      .and.callFake(mockLocalStorage.removeItem);
    spyOn(localStorage, 'clear')
      .and.callFake(mockLocalStorage.clear);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get token from localStorage', () => {
    const token = 'test';
    mockLocalStorage.setItem(key, token);
    expect(service.getToken()).toBe(token);
  });

  it('should call getToken method', () => {
    service.getToken();
    expect(localStorage.getItem).toHaveBeenCalledWith('token');
  });

  it('should get auth status', () => {
    const token = 'test';
    mockLocalStorage.setItem(key, token);
    expect(service.isAuthorized()).toBeTrue();
  });

  it('should call isAuthorized() method', () => {
    service.isAuthorized();
    expect(localStorage.getItem).toHaveBeenCalledWith('token');
  });

  it('should call saveToken() method', () => {
    service.saveToken('test');
    expect(localStorage.setItem).toHaveBeenCalledWith('token', 'test');
  });

  it('should call removeToken() method', () => {
    service.removeToken();
    expect(localStorage.removeItem).toHaveBeenCalledWith('token');
  });
});
