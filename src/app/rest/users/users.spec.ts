import { TestBed } from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

import { UsersRestService } from './users.service';

const usersList = [
  {
    id: '1212',
    name: 'test',
    age: 49
  },
  {
    id: '12123',
    name: 'test1',
    age: 50
  }
];

describe('UsersRestService', () => {
  let service: UsersRestService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UsersRestService]
    });
    service = TestBed.inject(UsersRestService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return user list', () => {
    service.getUsersList({})
      .subscribe(users => {
        expect(users.length).toBe(2);
        expect(users).toEqual(usersList);
      });

    const mock = httpTestingController.expectOne({
      url: '/users',
      method: 'GET'
    });

    mock.flush(usersList);

    httpTestingController.verify();
  });

  it('should not have a body', () => {
    service.getUsersList({})
      .subscribe();

    const mock = httpTestingController.expectOne(request => {
      expect(request.body).toBe(null);
      return true;
    });

    mock.flush(usersList);

    httpTestingController.verify();
  });

  it('should have group params', () => {
    service.getUsersList({group: 'test'})
      .subscribe();

    const mock = httpTestingController.expectOne(request => {
      expect(request.params.get('group')).toBe('test');
      return true;
    });

    mock.flush(usersList);

    httpTestingController.verify();
  });
});
