export type Params = {
  group?: string
};

export type Response = {
  id: string;
  name: string;
  age: number;
};
