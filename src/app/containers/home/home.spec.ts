import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Observable, of } from 'rxjs';

import {CommonModule} from '../../components/common/common.module';

import { Params, Response } from '../../rest/users/users.types';
import { UsersRestService } from '../../rest/users/users.service';

import { HomeComponent } from './home.component';

const usersList = [
  {
    id: '1212',
    name: 'test',
    age: 49
  },
  {
    id: '12123',
    name: 'test1',
    age: 50
  }
];

const userRestServiceStub: Partial<UsersRestService> = {
  getUsersList(params: Params): Observable<Response[]> {
    return of(usersList);
  }
};

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      providers: [UsersRestService],
      imports: [CommonModule]
    })
    .compileComponents();
    await TestBed.overrideProvider(UsersRestService, {useValue: userRestServiceStub});
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load users on init', () => {
    expect(component.users.length).toBe(0);
    fixture.detectChanges();
    expect(component.users.length).toBe(2);
  });

  it('should call to usersRestService.getUsersList service on init', () => {
    spyOn(userRestServiceStub, 'getUsersList')
      .and.returnValues(of(usersList));
    fixture.detectChanges();
    expect(userRestServiceStub.getUsersList).toHaveBeenCalledWith({ group: 'test' });
  });

  it('should display users on render', () => {
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelectorAll('app-card').length).toBe(2);
  });

  it('should render users with needed inputs', () => {
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    (compiled.querySelectorAll('app-card') || []).forEach((element, index) => {
      expect(element.getAttribute('ng-reflect-name')).toBe(usersList[index].name);
      expect(element.getAttribute('ng-reflect-age')).toBe(usersList[index].age.toString());
    });
  });
});
